#!/bin/sh
# Shell script para instalar GR-Foo e GR-802.15.4


echo "Selecione uma opção:"
echo "1 - Exibir data e hora do sistema"
echo "2 - Exibir o resultado da divisão 10/2"
echo "3 - Exibir uma mensagem"

read opcao;
case $opcao in
   "1")
      data=$(date +"%T, %d/%m/%y, %A")
      echo "$data"
      ;;
   "2")
     result=$((10/2))
     echo "divisao de 10/2 = $result"
   ;;
   "3")
    echo "Informe o seu nome:"
    read nome;
    echo "Bem-vindo ao mundo do shell script, $nome!"
;;
esac





# pausa=3

# echo “Qual o nome de uma de suas músicas favoritas?”
# read nome_musica;
# echo “Você gosta de ouvir $nome_musica!” 
# sleep $pausa
# echo "Ok"




# echo "\n Descompilando os arquivos... \n"
# sleep 3

# tar -vzxf ParaUpload.tar.gz

# ### GR-FOO ###
# echo "\n Instalando GR-FOO... \n"
# sleep 3

# cd ~ ;
# cd gr-foo ;
# mkdir build ;
# cd build ;
# cmake .. ;
# make ;
# sudo make install ;
# sudo ldconfig ;


# ### GR-IEEE802-15-4 ###
# echo "\n Instalando GR-IEEE802-15-4... \n"
# sleep 3

# cd ~ ;
# cd gr-ieee802-15-4 ;
# mkdir build ;
# cd build ;
# cmake .. ;
# make ;
# sudo make install ;
# sudo ldconfig ;

# ### GR-LQE ###
# echo "\n Instalando GR-LQE... \n"
# sleep 3

# cd ~ ;


# ### GRC Compile ###
# echo "\n Compilando ieee802_15_4_PHY.grc... \n"
# sleep 3

# cd ~ ;
# cd gr-ieee802-15-4/examples ;
# grcc ieee802_15_4_PHY.grc ;
#grcc -e transceiver.grc ;
