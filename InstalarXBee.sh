#!/bin/sh
# Shell script para instalar GR-Foo e GR-802.15.4

pausa=3

echo "\n Descompilando os arquivos... \n"
sleep $pausa

tar -vzxf ParaUpload.tar.gz

### GR-FOO ###
echo "\n Instalando GR-FOO... \n"
sleep $pausa

cd ~ ;
cd gr-foo ;
mkdir build ;
cd build ;
cmake .. ;
make ;
sudo make install ;
sudo ldconfig ;


### GR-IEEE802-15-4 ###
echo "\n Instalando GR-IEEE802-15-4... \n"
sleep $pausa

cd ~ ;
cd gr-ieee802-15-4 ;
mkdir build ;
cd build ;
cmake .. ;
make ;
sudo make install ;
sudo ldconfig ;

### GR-LQE ###
echo "\n Instalando GR-LQE... \n"
sleep $pausa

cd ~ ;


### GRC Compile ###
echo "\n Compilando ieee802_15_4_PHY.grc... \n"
sleep $pausa

cd ~ ;
cd gr-ieee802-15-4/examples ;
grcc ieee802_15_4_PHY.grc ;
#grcc -e transceiver.grc ;
